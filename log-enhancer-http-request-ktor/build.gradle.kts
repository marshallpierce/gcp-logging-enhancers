dependencies {
  implementation(kotlin("stdlib"))
  implementation(project(":log-enhancer-http-request"))

  api("io.ktor:ktor-server-core:2.2.4")
  testImplementation("io.ktor:ktor-server-test-host:2.2.4")
}
