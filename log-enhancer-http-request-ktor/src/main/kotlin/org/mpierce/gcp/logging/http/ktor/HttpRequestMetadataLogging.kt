package org.mpierce.gcp.logging.http.ktor

import com.google.cloud.logging.HttpRequest
import io.ktor.http.HttpMethod
import io.ktor.server.application.Application
import io.ktor.server.application.ApplicationCallPipeline
import io.ktor.server.application.BaseApplicationPlugin
import io.ktor.server.application.call
import io.ktor.server.plugins.origin
import io.ktor.server.request.httpMethod
import io.ktor.server.request.uri
import io.ktor.server.request.userAgent
import io.ktor.util.AttributeKey
import kotlinx.coroutines.asContextElement
import kotlinx.coroutines.withContext
import org.mpierce.gcp.logging.http.HttpRequestEnhancer

/**
 * Capture request metadata so it's accessible for [HttpRequestEnhancer].
 */
class HttpRequestMetadataLogging {
    companion object Feature : BaseApplicationPlugin<Application, Unit, HttpRequestMetadataLogging> {
        override val key: AttributeKey<HttpRequestMetadataLogging> = AttributeKey("Http request metadata thread local")

        override fun install(pipeline: Application, configure: Unit.() -> Unit): HttpRequestMetadataLogging {
            val feature = HttpRequestMetadataLogging()

            pipeline.intercept(ApplicationCallPipeline.Plugins) {
                val httpReq = HttpRequest.newBuilder().run {
                    val method = when (call.request.httpMethod) {
                        HttpMethod.Get -> HttpRequest.RequestMethod.GET
                        HttpMethod.Head -> HttpRequest.RequestMethod.HEAD
                        HttpMethod.Post -> HttpRequest.RequestMethod.POST
                        HttpMethod.Put -> HttpRequest.RequestMethod.PUT
                        // RequestMethod only has prebuilt instances for a few headers
                        else -> HttpRequest.RequestMethod.valueOf(call.request.httpMethod.value)
                    }

                    // other request to potentially populate:
                    // request size
                    // and also a bunch of response stuff:
                    // status
                    // response seize
                    // latency
                    // cacheLookup
                    // cacheHit
                    // cacheValidated
                    // cacheFillBytes
                    // protocol

                    setRequestMethod(method)
                    setRequestUrl(call.request.uri)
                    setUserAgent(call.request.userAgent())
                    setRemoteIp(call.request.origin.remoteHost)
                    build()
                }

                withContext(HttpRequestEnhancer.HTTP_REQUEST_METADATA.asContextElement(httpReq)) {
                    proceed()
                }
            }

            return feature
        }
    }
}
