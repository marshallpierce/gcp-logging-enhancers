package org.mpierce.gcp.logging.http.ktor

import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.testing.ApplicationTestBuilder
import io.ktor.server.testing.testApplication
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mpierce.gcp.logging.http.HttpRequestEnhancer

internal class HttpRequestMetadataLoggingTest {
    @Test
    internal fun threadLocalIsSet() {
        val appInit: ApplicationTestBuilder.() -> Unit = {
            install(HttpRequestMetadataLogging)
            install(Routing) {
                get("/foo") {
                    call.respond(
                        HttpStatusCode.OK,
                        "URL: ${HttpRequestEnhancer.HTTP_REQUEST_METADATA.get()?.requestUrl}"
                    )
                }
            }
        }

        testApplication {
            appInit()

            val response = client.get("/foo?bar=baz")

            Assertions.assertEquals(HttpStatusCode.OK, response.status)
            Assertions.assertEquals("URL: /foo?bar=baz", response.bodyAsText())
        }
    }
}
