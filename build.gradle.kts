import java.net.URI
import java.time.Duration

plugins {
    `java-library`
    id("net.researchgate.release") version "3.0.2"
    id("io.github.gradle-nexus.publish-plugin") version "1.2.0"
    id("com.github.ben-manes.versions") version "0.46.0"
    kotlin("jvm") version "1.8.10" apply false
    id("org.jetbrains.dokka") version "1.7.20" apply false
    id("org.jmailen.kotlinter") version "3.13.0" apply false
}

subprojects {
    apply(plugin = "maven-publish")
    apply(plugin = "signing")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jmailen.kotlinter")

    repositories {
        mavenCentral()
    }

    dependencies {
        api("com.google.cloud:google-cloud-logging-logback:0.121.2-alpha")

        testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.2")
    }

    configure<JavaPluginExtension> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
        withSourcesJar()
    }

    tasks {
        named<Test>("test") {
            useJUnitPlatform()
        }

        withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
            kotlinOptions.jvmTarget = "1.8"
            kotlinOptions.freeCompilerArgs = listOf("-progressive")
        }
    }

    if (name == "log-enhancer-http-request-ktor") {
        apply(plugin = "org.jetbrains.dokka")

        // kotlin subprojects do their own thing
        tasks.register<Jar>("docJar") {
            from(tasks["dokkaHtml"].outputs)
            archiveClassifier.set("javadoc")
        }
    } else {
        tasks.register<Jar>("docJar") {
            from(tasks["javadoc"])
            archiveClassifier.set("javadoc")
        }
    }

    group = "org.mpierce.gcp.logging"

    configure<PublishingExtension> {
        publications {
            register<MavenPublication>("sonatype") {
                from(components["java"])
                artifact(tasks["docJar"])
                // sonatype required pom elements
                pom {
                    name.set("${group}:${name}")
                    description.set(name)
                    url.set("https://bitbucket.org/marshallpierce/gcp-logging-enhancers")
                    licenses {
                        license {
                            name.set("Copyfree Open Innovation License 0.5")
                            url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                        }
                    }
                    developers {
                        developer {
                            id.set("marshallpierce")
                            name.set("Marshall Pierce")
                            email.set("575695+marshallpierce@users.noreply.github.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:https://bitbucket.org/marshallpierce/gcp-logging-enhancers")
                        developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/gcp-logging-enhancers.git")
                        url.set("https://bitbucket.org/marshallpierce/gcp-logging-enhancers")
                    }
                }
            }
        }
        // A safe throw-away place to publish to:
        // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
        repositories {
            maven {
                name = "localDebug"
                url = URI.create("file:///${project.buildDir}/repos/localDebug")
            }
        }
    }

    // don't barf for devs without signing set up
    if (project.hasProperty("signing.keyId")) {
        configure<SigningExtension> {
            sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
        }
    }

    // releasing should publish
    rootProject.tasks.afterReleaseBuild {
        dependsOn(provider { tasks.named("publishToSonatype") })
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}

release {
    git {
        requireBranch.set("master")
    }
}
