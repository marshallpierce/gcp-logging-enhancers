package org.mpierce.gcp.logging.http;

import com.google.cloud.logging.HttpRequest;
import com.google.cloud.logging.LogEntry;
import com.google.cloud.logging.LoggingEnhancer;

/**
 * If an HttpRequest is set in the thread local, set it in the LogEntry.
 */
public final class HttpRequestEnhancer implements LoggingEnhancer {

    /**
     * Set (and un-set) request metadata as appropriate.
     */
    public static final ThreadLocal<HttpRequest> HTTP_REQUEST_METADATA = new ThreadLocal<>();

    @Override
    public void enhanceLogEntry(LogEntry.Builder builder) {
        HttpRequest req = HTTP_REQUEST_METADATA.get();
        if (req != null) {
            builder.setHttpRequest(req);
        }
    }
}

