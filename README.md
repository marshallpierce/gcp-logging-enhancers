[ ![Download](https://api.bintray.com/packages/marshallpierce/maven/org.mpierce.gcp.logging%3Agcp-logging-enhancers/images/download.svg) ](https://bintray.com/marshallpierce/maven/org.mpierce.gcp.logging%3Agcp-logging-enhancers/_latestVersion)

# Extensions for the Logback Appender for Google Cloud Logging

Google Cloud [Logging](https://cloud.google.com/logging/), aka Stackdriver Logging, is a log aggregation service. Google's existing `google-cloud-java` project already has a [Logback]() [appender implementation](https://github.com/googleapis/google-cloud-java/tree/master/google-cloud-clients/google-cloud-contrib/google-cloud-logging-logback), so it's easy to use with Logback (and therefore with SLF4J), but it's pretty bare bones. Fortunately, they offer a way to customize [`LogEntry`](https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry) instances fed to the logging service. 

### HTTP requests

The `LogEntry` type has special support for [HTTP request metadata](https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#httprequest).

- `HttpRequestEnhancer` adds request metadata to the `LogEntry`, if any metadata has been set in a corresponding ThreadLocal. This type is in the `org.mpierce.gcp.logging:log-enhancer-http-request` artifact.
    - By itself, this does nothing: you need to populate the thread local for your specific HTTP setup (e.g. if you're using Servlets, you probably want to write a `Filter` that manages the ThreadLocal).
    - If you're using [Ktor](https://ktor.io/), `HttpRequestMetadataLogging` (in `org.mpierce.gcp.logging:log-enhancer-http-request-ktor`) is a Ktor [feature](https://ktor.io/servers/features.html) that sets request metadata appropriately so that `HttpRequestEnhancer` can use it. It needs no configuration, so just `install()` it in your application setup.

# Usage

Artifacts are available in [jcenter](https://bintray.com/bintray/jcenter?filterByPkgName=org.mpierce.gcp.logging%3Agcp-logging-enhancers).

 There are two interfaces used by the Logback appender: `LoggingEnhancer` and `LoggingEventEnhancer` (not currently used by this library, but documented here in case it's useful). Look at the class you want to use and note which interface it implements. If you want to use a `LoggingEnhancer`, add XML like this in your logback config file in the `<appender>` block for `com.google.cloud.logging.logback.LoggingAppender`:
 
```
<enhancer>fully.qualified.class.name.Here</enhancer>
```
 
If it's a `LoggingEventEnhancer`, use this:
 
```
<loggingEventEnhancer>fully.qualified.class.name.Here</loggingEventEnhancer>
```

Add `debug="true"` to your `<configuration>` element to help debug issues.

Here's an example Logback config:

```
<configuration>
  <contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator"/>

  <appender name="GCP" class="com.google.cloud.logging.logback.LoggingAppender">
    <enhancer>org.mpierce.gcp.logging.http.HttpRequestEnhancer</enhancer>
    <flushLevel>WARN</flushLevel>
  </appender>

  <root level="debug">
    <appender-ref ref="GCP"/>
  </root>
</configuration>
```

